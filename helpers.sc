inline to-pointer-count (arg)
    static-if (none? arg)
        _ null 0
    else
        let count =
            countof arg
        let ptr =
            reftoptr (arg @ 0)
        _ ptr count

inline make-typecall (result-type modifier create-function)
    inline (ty args...)
        bitcast (modifier create-function args...) result-type

locals;
