sugar redef-type (typename convert-args)
    qq define [typename]
        let inner-type = [typename]
        unlet [typename]
        typedef [typename] : (storageof inner-type)
            inline __typecall (ty args...)
                let args... =
                    [convert-args] args...
                let inner-value =
                    inner-type args...
                let result =
                    bitcast inner-value this-type
                result
            inline native (self)
                bitcast self inner-type
            inline __imply (vT T)
                static-if (T == inner-type)
                    inline "descriptor-imply" (self)
                        native self

run-stage;

using import utils
using import .helpers
import .native
using native
import .wrappers

inline id (args...) args...

do
    redef-type RequestAdapterOptions
        inline (power_preference compatible_surface)
            _
                compatible_surface =
                    'native compatible_surface
                keyed power_preference
    redef-type DeviceDescriptor id
    redef-type BlendDescriptor id
    redef-type BufferDescriptor id
    redef-type ColorStateDescriptor id
    redef-type CommandEncoderDescriptor id
    redef-type DepthStencilStateDescriptor id
    redef-type RasterizationStateDescriptor id
    redef-type RenderPassColorAttachmentDescriptor id
    redef-type SamplerDescriptor id
    redef-type ShaderModuleDescriptor id
    redef-type StencilStateFaceDescriptor id
    redef-type SwapChainDescriptor id
    redef-type TextureDescriptor id
    redef-type TextureViewDescriptor id
    let VertexAttributeDescriptor_native = VertexAttributeDescriptor
    redef-type VertexAttributeDescriptor id
    let VertexBufferLayoutDescriptor_native = VertexBufferLayoutDescriptor
    redef-type VertexBufferLayoutDescriptor
        inline (stride step_mode attributes)
            let attributes attributes_length =
                static-if ((typeof attributes) == VertexAttributeDescriptor)
                    _ (reftoptr ('native attributes)) 1
                else
                    let ptr count =
                        to-pointer-count attributes
                    _ (bitcast ptr (pointer VertexAttributeDescriptor_native)) count
            keyed stride step_mode attributes attributes_length

    redef-type BindGroupLayoutDescriptor
        inline (label entries)
            _
                label = label
                static-if ((typeof entries) == BindGroupLayoutEntry)
                    _ &entries 1
                else
                    to-pointer-count entries

    redef-type PipelineLayoutDescriptor
        inline (bind_group_layouts)
            static-if ((typeof bind_group_layouts) == wrappers.BindGroupLayout)
                _ (reftoptr ('native bind_group_layouts)) 1
            else
                let ptr count =
                    to-pointer-count bind_group_layouts
                _ (bitcast ptr (pointer BindGroupLayoutId)) count

    redef-type RenderPipelineDescriptor
        inline (layout color_states color_states_length vertex_stage fragment_stage rasterization_state depth_stencil_state vertex_state rest...)
            static-assert (none? color_states_length) "Length not supported. Use an array type instead."
            let color_states color_states_length =
                do
                    let ptr count =
                        static-if ((typeof color_states) == ColorStateDescriptor)
                            _
                                reftoptr color_states
                                1
                        else
                            to-pointer-count color_states
                    let ptr =
                        bitcast ptr (pointer native.ColorStateDescriptor)
                    _ ptr count
            _
                layout =
                    'native layout
                vertex_stage =
                    'native vertex_stage
                fragment_stage =
                    reftoptr
                        'native fragment_stage
                rasterization_state =
                    reftoptr
                        'native rasterization_state
                color_states = color_states
                color_states_length = color_states_length
                depth_stencil_state =
                    do
                        static-if (none? depth_stencil_state) null
                        else
                            reftoptr
                                'native depth_stencil_state
                vertex_state = vertex_state
                rest...

    redef-type ComputePipelineDescriptor
        inline (layout rest...)
            _
                layout =
                    'native layout
                rest...

    redef-type ProgrammableStageDescriptor
        inline (module rest...)
            _
                module =
                    'native module
                rest...

    redef-type VertexStateDescriptor
        inline (vertex_buffers rest...)
            _
                call
                    va-join
                        static-if ((typeof vertex_buffers) == VertexBufferLayoutDescriptor)
                            _
                                vertex_buffers = (reftoptr ('native vertex_buffers))
                                vertex_buffers_length = 1
                        else
                            let ptr count =
                                to-pointer-count vertex_buffers
                            _
                                vertex_buffers = (bitcast ptr (pointer VertexBufferLayoutDescriptor_native))
                                vertex_buffers_length = count
                    rest...

    redef-type RenderPassDescriptor
        inline (color_attachments depth_stencil_attachment)
            _
                call
                    va-join
                        do
                            let ptr count =
                                static-if ((typeof color_attachments) == RenderPassColorAttachmentDescriptor)
                                    _
                                        reftoptr color_attachments
                                        color_attachments_length = 1
                                else
                                    to-pointer-count color_attachments
                            let ptr =
                                bitcast ptr (pointer native.RenderPassColorAttachmentDescriptor)
                            _
                                color_attachments = ptr
                                color_attachments_length = count
                    depth_stencil_attachment =
                        do
                            static-if depth_stencil_attachment
                                &depth_stencil_attachment
                            else null

    redef-type BufferBinding
        inline (buffer rest...)
            _
                buffer =
                    'native buffer
                rest...

    redef-type BindGroupEntry
        inline (binding resource)
            _
                binding = binding
                resource =
                    'native resource

    redef-type BindGroupDescriptor
        inline (layout label entries)
            _
                layout =
                    'native layout
                do
                    let ptr count =
                        static-if ((typeof entries) == BindGroupEntry)
                            _ &entries 1
                        else
                            to-pointer-count entries
                    let ptr =
                        bitcast ptr (pointer native.BindGroupEntry)
                    _
                        label = label
                        entries = ptr
                        entries_length = count
    locals;

