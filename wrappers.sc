sugar define-wrapper (name unique? modifier create-function body...)
    let storage-type = (Symbol (name as Symbol as string .. "Id"))
    qq typedef [name] [unique?] (storageof [storage-type])
        let __typecall = (make-typecall this-type [modifier create-function])
        inline native (self)
            bitcast (view self) [storage-type]
        inline __imply (vT T)
            static-if (T == [storage-type])
                inline "main-type-imply" (self)
                    native self
        unquote-splice body...

run-stage;

using import .helpers

using import .native

inline manual (create-function super args...)
    create-function super args...

inline single (create-function super)
    create-function
        'native super

inline default (create-function super desc)
    create-function
        'native super
        & ('native desc)

inline double (create-function super second desc)
    create-function
        'native super
        'native second
        & ('native desc)

do
    let Adapter = AdapterId

    typedef Surface : (storageof SurfaceId)
        inline __typecall (ty kind args...)
            bitcast
                call
                    static-match kind
                    case 'xlib
                        create_surface_from_xlib
                    case 'windows-hwnd
                        create_surface_from_windows_hwnd
                    case 'macos-layer
                        create_surface_from_metal_layer
                    default
                        static-error "Window backend not supported"
                    args...
                this-type
        inline native (self)
            bitcast (view self) SurfaceId


    # Device

    define-wrapper Device :: manual
        inline (super adapter path)
            adapter_request_device super
                & ('native adapter)
                static-if path
                    path
                else
                    null
        inline __drop (self)
            device_destroy ('native self)
        inline poll (self force-wait)
            device_poll ('native self) force-wait

    typedef CommandBuffer : (storageof CommandBufferId)
    define-wrapper Queue : single device_get_default_queue
        inline submit (self command-buffers)
            queue_submit
                'native self
                static-if ((typeof command-buffers) == CommandBuffer)
                    _ (bitcast &command-buffers (pointer CommandBufferId)) 1
                else
                    let ptr count =
                        to-pointer-count command-buffers
                    _ (bitcast ptr (pointer CommandBufferId)) count


    # Resource

    typedef Buffer :: (storageof BufferId)
        inline __typecall (ty super desc T)
            let result rest... =
                static-if (none? T)
                    device_create_buffer
                        'native super
                        & ('native desc)
                else
                    local data : (mutable pointer T)
                    _
                        device_create_buffer_mapped
                            'native super
                            & ('native desc)
                            bitcast &data (mutable pointer (mutable pointer u8))
                        data
            _
                bitcast result this-type
                rest...
        inline native (self)
            bitcast (view self) BufferId
        inline unmap (self)
            buffer_unmap ('native self)
        inline map-read-async (self rest...)
            buffer_map_read_async ('native self) rest...
        inline map-write-async (self rest...)
            buffer_map_write_async ('native self) rest...
        inline __drop (self)
            buffer_destroy ('native self)

    define-wrapper TextureView :: default texture_create_view
        inline __drop (self)
            texture_view_destroy ('native self)

    define-wrapper Texture :: default device_create_texture
        inline __drop (self)
            texture_destroy ('native self)

    unlet Sampler; define-wrapper Sampler : default device_create_sampler


    # Binding model

    define-wrapper BindGroupLayout : default device_create_bind_group_layout

    define-wrapper PipelineLayout : default device_create_pipeline_layout

    define-wrapper BindGroup :: default device_create_bind_group
        inline __drop (self)
            bind_group_destroy ('native self)


    # Pipeline

    define-wrapper ShaderModule : default device_create_shader_module

    define-wrapper RenderPipeline : default device_create_render_pipeline

    define-wrapper ComputePipeline : default device_create_compute_pipeline


    # Command

    define-wrapper CommandEncoder : default device_create_command_encoder
        inline copy-buffer-to-buffer (self src src_offset dst dst_offset size)
            command_encoder_copy_buffer_to_buffer
                'native self
                'native src
                src_offset
                'native dst
                dst_offset
                size

        inline make-copy (method)
            inline "copier" (self source destination copy_size)
                method
                    'native self
                    reftoptr source
                    reftoptr destination
                    copy_size
        let copy-buffer-to-texture copy-texture-to-buffer copy-texture-to-texture =
            va-map make-copy
                command_encoder_copy_buffer_to_texture
                command_encoder_copy_texture_to_buffer
                command_encoder_copy_texture_to_texture
        unlet make-copy

    typedef+ CommandBuffer
        inline __typecall (ty super arg)
            bitcast
                static-match (typeof super)
                case CommandEncoder
                    command_encoder_finish ('native super)
                        static-if (none? arg)
                            null
                        else
                            reftoptr arg
                default
                    static-error
                        "CommandBuffer cannot be created from this type" .. (tostring (typeof super))
                this-type
        inline native (self)
            bitcast (view self) CommandBufferId

        inline copy-buffer-to-buffer (self src src_offset dst dst_offset size)
            command_encoder_copy_buffer_to_buffer
                'native self
                'native src
                src_offset
                'native dst
                dst_offset
                size

        inline make-copy (method)
            inline "copier" (self source destination copy_size)
                method
                    'native self
                    reftoptr source
                    reftoptr destination
                    copy_size
        let copy-buffer-to-texture copy-texture-to-buffer copy-texture-to-texture =
            va-map make-copy
                command_encoder_copy_buffer_to_texture
                command_encoder_copy_texture_to_buffer
                command_encoder_copy_texture_to_texture
        unlet make-copy

    define-wrapper RenderPass : default command_encoder_begin_render_pass
        inline set-pipeline (self pipeline)
            render_pass_set_pipeline
                'native self
                'native pipeline
        inline set-bind-group (self index bind-group offsets)
            render_pass_set_bind_group
                'native self
                index
                'native bind-group
                to-pointer-count offsets

        inline insert-debug-marker (self label)
            render_pass_insert_debug_marker
                'native self
                label as rawstring

        inline pop-debug-group (self)
            render_pass_pop_debug_group
                'native self


        inline push-debug-group (self label)
            render_pass_push_debug_group
                'native self
                label as rawstring

        inline set-blend-color (self color)
            render_pass_set_blend_color
                'native self
                label as rawstring

        inline set-index-buffer (self buffer offset)
            render_pass_set_index_buffer
                'native self
                'native buffer
                offset

        inline set-scissor-rect (self args...)
            render_pass_set_scissor_rect
                'native self
                args...

        inline set-stencil-reference (self args...)
            render_pass_set_stencil_reference
                'native self
                args...

        #void wgpu_render_pass_set_vertex_buffers(WGPURenderPassId pass_id,
                                         const WGPUBufferId *buffer_ptr,
                                         const WGPUBufferAddress *offset_ptr,
                                         uintptr_t count);

        inline set-viewport (self args...)

            render_pass_set_viewport
                'native self
                args...

        inline draw (self args...)
            render_pass_draw ('native self) args...

        inline draw-indexed (self args...)
            render_pass_draw_indexed ('native self) args...

        inline draw-indirect (self args...)
            render_pass_draw_indirect ('native self) args...

        inline draw-indexed-indirect (self args...)
            render_pass_draw_indexed_indirect ('native self) args...

        inline end-pass (self)
            render_pass_end_pass
                'native self
            ;

    define-wrapper ComputePass : single command_encoder_begin_compute_pass
        inline set-pipeline (self pipeline)
            compute_pass_set_pipeline
                'native self
                'native pipeline
        inline set-bind-group (self index bind-group offsets)
            compute_pass_set_bind_group ('native self) index
                'native bind-group
                to-pointer-count offsets

        inline dispatch (self args...)
            compute_pass_dispatch ('native self) args...

        inline insert-debug-marker (self label)
            compute_pass_insert_debug_marker
                'native self
                label as rawstring

        inline pop-debug-group (self)
            compute_pass_pop_debug_group
                'native self


        inline push-debug-group (self label)
            compute_pass_push_debug_group
                'native self
                label as rawstring

        inline end-pass (self)
            compute_pass_end_pass
                'native self
            ;

    let RenderBundle = RenderBundleId


    # Swap chain

    define-wrapper SwapChain : double device_create_swap_chain
        inline present (self)
            swap_chain_present
                'native self

        inline get-next-texture (self)
            let output =
                swap_chain_get_next_texture
                    'native self
            _ output.status output.view_id



    locals;

