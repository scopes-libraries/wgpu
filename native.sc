using import utils.bindings
using import utils

load-library "/usr/local/lib/libwgpu_native.so"

vvv bind scope
vvv include
""""
    #include <wgpu.h>

    WGPUBindingResource wgpu_buffer_binding_create_resource(WGPUBufferBinding* buffer_binding) {
        return (WGPUBindingResource) {
            .tag = WGPUBindingResource_Buffer,
            .buffer = (WGPUBindingResource_WGPUBuffer_Body) { *buffer_binding }
        };
    }

    WGPUBindingResource wgpu_sampler_create_resource(WGPUSamplerId sampler) {
        return (WGPUBindingResource) {
            .tag = WGPUBindingResource_Sampler,
            .sampler = (WGPUBindingResource_WGPUSampler_Body) { sampler }
        };
    }

    WGPUBindingResource wgpu_texture_view_create_resource(WGPUTextureViewId texture_view) {
        return (WGPUBindingResource) {
            .tag = WGPUBindingResource_TextureView,
            .texture_view = (WGPUBindingResource_WGPUTextureView_Body) { texture_view }
        };
    }

let new-scope = (Scope)

vvv bind new-scope
copy-bindings scope.typedef
    prefix-remover "wgpu"
    new-scope
vvv bind new-scope
copy-bindings scope.extern
    prefix-remover "wgpu"
    new-scope
vvv bind new-scope
copy-bindings scope.define
    prefix-remover "wgpu"
    new-scope
vvv bind new-scope
copy-bindings scope.struct
    prefix-remover "wgpu"
    new-scope
vvv bind new-scope
copy-bindings scope.enum
    prefix-remover "wgpu"
    new-scope
vvv bind new-scope
copy-bindings scope.const
    prefix-remover "wgpu"
    new-scope
vvv bind new-scope
copy-bindings scope.union
    prefix-remover "wgpu"
    new-scope

for k v in new-scope
    if (('typeof v) == type and (v as type) < CEnum)
        copy-bindings (v as type)
            prefix-remover ("WGPU" .. k as Symbol as string)

let ShaderStage BufferUsage TextureUsage ColorWrite TextureAspectFlags =
    copy-bindings new-scope
        prefix-remover "ShaderStage"
        Scope;
    copy-bindings new-scope
        prefix-remover "BufferUsage"
        Scope;
    copy-bindings new-scope
        prefix-remover "TextureUsage"
        Scope;
    copy-bindings new-scope
        prefix-remover "ColorWrite"
        Scope;
    copy-bindings new-scope
        prefix-remover "TextureAspectFlags"
        Scope;

define new-scope
    'bind-symbols new-scope
        keyed ShaderStage BufferUsage TextureUsage ColorWrite TextureAspectFlags

run-stage;

typedef+ new-scope.Color
    let TRANSPARENT BLACK WHITE RED GREEN BLUE =
        new-scope.Color 0 0 0 0
        new-scope.Color 0 0 0 1
        new-scope.Color 1 1 1 1
        new-scope.Color 1 0 0 1
        new-scope.Color 0 1 0 1
        new-scope.Color 0 0 1 1

new-scope
