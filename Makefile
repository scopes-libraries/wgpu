.RECIPEPREFIX+=*

SCOPES_INSTALL_DIRECTORY=/usr/local/lib/scopes

.PHONY: check install triangle triangle-example

check: init.sc
* scopes init.sc

triangle:
* scopes examples/triangle.sc

triangle-native:
* scopes examples/triangle-native.sc

install:
* rm -f $(SCOPES_INSTALL_DIRECTORY)/wgpu
* ln -s $(shell pwd) $(SCOPES_INSTALL_DIRECTORY)/wgpu

