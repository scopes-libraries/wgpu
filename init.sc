import .native
import .wrappers
import .descriptors
import .misc

define special
    let inner-type = native.BindingResource
    typedef BindingResource : (storageof inner-type)
        inline... create
        case (buffer-binding : descriptors.BufferBinding,)
            native.buffer_binding_create_resource
                & ('native buffer-binding)
        case (sampler : wrappers.Sampler,)
            native.sampler_create_resource
                'native sampler
        case (texture-view : wrappers.TextureView,)
            native.texture_view_create_resource
                'native texture-view

        inline __typecall (ty args...)
            let result =
                create args...
            bitcast result this-type
        unlet create

        inline native (self)
            bitcast self inner-type
        inline __imply (vT T)
            static-if (T == inner-type)
                inline "descriptor-imply" (self)
                    native self
    locals;

define functions
    using native
    do
        inline request-adapter-async (desc mask callback userdata)
            request_adapter_async
                reftoptr
                    'native desc
                mask
                static-typify callback AdapterId voidstar
                &userdata
        locals;

import .enums

.. wrappers descriptors misc special functions enums

