# Information

[Scopes](http://scopes.rocks) wrapper of the [wgpu](https://github.com/gfx-rs/wgpu-native) graphics library.
It contains the native wrapper, which uses the same semantics as the C binding, and a more advanced wrapper.
The C example is also ported for each wrapper.

It's necessary to install [utils](https://gitlab.com/scopes-libraries/utils) to use this library.

Since scopes can be compiled to Spir-V, it's a perfect match for GPU-Programming.

## Currently required Versions

* Scopes: 0.16
* Utils: 0.1
* WGPU: v0.5.2

# Wrappers

## Native Wrapper

The native wrapper also supports additional features, which don't change the existing functionality:
* no use of wgpu prefix
* enum types also don't use the enum prefix again
* bitflag enum types are implemented as scopes
* color constants are implemented in types
* tagged union for resource has to be created using new methods having the same naming conventions as wgpu functions (`wgpu_{buffer_binding,sampler,texture_view}_create_resource`)

The native wrapper will never support new ways to call functions besides the removed wgpu prefix.

### Example

The example has a few more differences:
* shaders are written in scopes as well and compiled to Spir-V at compile time
* instead of having a main function, the program just starts
* errors are generated using error specific functions

## Advanced Wrapper

The advanced wrapper contains a few more advanced changes to the native wrapper.

* for each ID type, there is a distinct unique type
* functions, that return an id are used as constructors for these new types
* destroy functions are used as destructors
* all other functions are implemented as methods of the specific types, dropping the type prefix, or functions, when no type is applicable, while converting "_" to "-"
* functions take references instead of pointers
* types and functions taking pointer-count-pairs take some kind of array, a single reference or nothing
* BindingResource constructor infers enum tag from supplied type

Advanced borrow checking with recursive dependencies is not supported in scopes.
If scopes will support that, it's not clear, weather to include it into the advanced wrapper or create an even more advanced wrapper.
