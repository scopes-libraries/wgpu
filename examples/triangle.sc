using import .shaders

load-library "/usr/lib/libglfw.so.3"

import ..init

let wgpu = init

let p = pointer

define glfw
    include
        """"#include <GLFW/glfw3.h>
            #define GLFW_EXPOSE_NATIVE_X11
            #define GLFW_EXPOSE_NATIVE_WAYLAND
            #include <GLFW/glfw3native.h>

define glfw
    .. glfw.extern glfw.define

run-stage;

using glfw

let pointer = p
unlet p

if (not (glfwInit))
    error "Cannot initialize glfw"

glfwWindowHint GLFW_CLIENT_API GLFW_NO_API

let window =
    glfwCreateWindow 640 480 "wgpu with glfw" null null

if (window == null)
    error "Cannot create window"

define surface
    let x11_display = (glfwGetX11Display)
    let x11_window = (glfwGetX11Window window)
    wgpu.Surface 'xlib
        bitcast x11_display (mutable pointer voidstar)
        x11_window

fn request-adapter-callback (received userdata)
    (ptrtoref (bitcast userdata (mutable pointer wgpu.Adapter))) = received

local adapter : wgpu.Adapter 0

wgpu.request-adapter-async
    local wgpu.RequestAdapterOptions
        power_preference = wgpu.PowerPreference.LowPower
        compatible_surface = surface
    2 | 4 | 8
    request-adapter-callback
    adapter

let device =
    wgpu.Device adapter
        local wgpu.DeviceDescriptor
            extensions =
                typeinit
                    anisotropic_filtering = false
            limits =
                typeinit
                    max_bind_groups = 1

let vertex-shader =
    wgpu.ShaderModule device
        local wgpu.ShaderModuleDescriptor
            code = vert

let fragment-shader =
    wgpu.ShaderModule device
        local wgpu.ShaderModuleDescriptor
            code = frag

local bind-group-layout =
    wgpu.BindGroupLayout device
        local wgpu.BindGroupLayoutDescriptor
            label = "bind group layout"

let bind-group =
    wgpu.BindGroup device
        local wgpu.BindGroupDescriptor
            label = "bind group"
            layout = bind-group-layout

let pipeline-layout =
    wgpu.PipelineLayout device
        local wgpu.PipelineLayoutDescriptor
            bind_group_layouts = bind-group-layout

let render-pipeline =
    wgpu.RenderPipeline device
        local wgpu.RenderPipelineDescriptor
            layout = pipeline-layout
            vertex_stage =
                wgpu.ProgrammableStageDescriptor
                    module = vertex-shader
                    entry_point = "main"
            fragment_stage =
                local wgpu.ProgrammableStageDescriptor
                    module = fragment-shader
                    entry_point = "main"
            rasterization_state =
                local wgpu.RasterizationStateDescriptor
                    front_face = wgpu.FrontFace.Ccw
                    cull_mode = wgpu.CullMode.None
                    depth_bias = 0
                    depth_bias_slope_scale = 0.0
                    depth_bias_clamp = 0.0
            primitive_topology = wgpu.PrimitiveTopology.TriangleList
            color_states =
                local wgpu.ColorStateDescriptor
                    format = wgpu.TextureFormat.Bgra8Unorm
                    alpha_blend =
                        wgpu.BlendDescriptor
                            src_factor = wgpu.BlendFactor.One
                            dst_factor = wgpu.BlendFactor.Zero
                            operation = wgpu.BlendOperation.Add
                    color_blend =
                        wgpu.BlendDescriptor
                            src_factor = wgpu.BlendFactor.One
                            dst_factor = wgpu.BlendFactor.Zero
                            operation = wgpu.BlendOperation.Add
                    write_mask = wgpu.ColorWrite.ALL
            vertex_state =
                wgpu.VertexStateDescriptor
                    index_format = wgpu.IndexFormat.Uint16
            sample_count = 1

local prev_width = 0
local prev_height = 0
glfwGetWindowSize window &prev_width &prev_height

local swap-chain =
    wgpu.SwapChain device surface
        local wgpu.SwapChainDescriptor
            usage = wgpu.TextureUsage.OUTPUT_ATTACHMENT
            format = wgpu.TextureFormat.Bgra8Unorm
            width =
                prev_width as u32
            height =
                prev_height as u32
            present_mode = wgpu.PresentMode.Fifo

let queue =
    wgpu.Queue device

while (not (glfwWindowShouldClose window))
    local width = 0
    local height = 0
    glfwGetWindowSize window &width &height
    if (width != prev_width or height != prev_height)
        prev_width = width
        prev_height = height

        swap-chain =
            wgpu.SwapChain device surface
                local wgpu.SwapChainDescriptor
                    usage = wgpu.TextureUsage.OUTPUT_ATTACHMENT
                    format = wgpu.TextureFormat.Bgra8Unorm
                    width = (width as u32)
                    height = (height as u32)
                    present_mode = wgpu.PresentMode.Fifo

    let _ attachment =
        'get-next-texture swap-chain

    let cmd-encoder =
        wgpu.CommandEncoder device
            local wgpu.CommandEncoderDescriptor
                label = "command encoder"

    local color-attachment : wgpu.RenderPassColorAttachmentDescriptor
        attachment = attachment
        load_op = wgpu.LoadOp.Clear
        store_op = wgpu.StoreOp.Store
        clear_color = wgpu.Color.GREEN


    let rpass =
        wgpu.RenderPass cmd-encoder
            local wgpu.RenderPassDescriptor
                color_attachments = color-attachment

    'set-pipeline rpass render-pipeline
    'set-bind-group rpass 0 bind-group
    'draw rpass 3 1 0 0
    'end-pass rpass

    local cmd-buf =
        wgpu.CommandBuffer cmd-encoder
    'submit queue cmd-buf

    'present swap-chain

    glfwPollEvents;


glfwDestroyWindow window
glfwTerminate;

