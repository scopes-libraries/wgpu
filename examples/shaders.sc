using import glsl
using import glm

fn vert ()
    local positions =
        arrayof vec2
            vec2 0.0 -0.5
            vec2 0.5 0.5
            vec2 -0.5 0.5
    gl_Position =
        vec4
            positions @ gl_VertexIndex
            0.0
            1.0
    _;

out out-color : vec4
    location = 0

fn frag ()
    out-color =
        vec4 1 0 0 1
    _;

let vert frag =
    compile-spirv 'vertex (typify vert)
    compile-spirv 'fragment (typify frag)

locals;
