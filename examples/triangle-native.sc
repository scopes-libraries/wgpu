using import .shaders

load-library "/usr/lib/libglfw.so.3"

import ..native

let wgpu = native

let p = pointer

vvv bind glfw
include
    """"#include <GLFW/glfw3.h>
        #define GLFW_EXPOSE_NATIVE_X11
        #define GLFW_EXPOSE_NATIVE_WAYLAND
        #include <GLFW/glfw3native.h>

define glfw
    .. glfw.extern glfw.define

run-stage;

using glfw

let pointer = p
unlet p

if (not (glfwInit))
    error "Cannot initialize glfw"

glfwWindowHint GLFW_CLIENT_API GLFW_NO_API

let window =
    glfwCreateWindow 640 480 "wgpu with glfw" null null

if (window == null)
    error "Cannot create window"

define surface
    let x11_display = (glfwGetX11Display)
    let x11_window = (glfwGetX11Window window)
    wgpu.create_surface_from_xlib
        bitcast x11_display (mutable pointer voidstar)
        x11_window

local adapter : wgpu.AdapterId 0

fn request_adapter_callback (received userdata)
    (ptrtoref (bitcast userdata (mutable pointer wgpu.AdapterId))) = received

wgpu.request_adapter_async
    reftoptr
        local wgpu.RequestAdapterOptions
            power_preference = wgpu.PowerPreference.LowPower
            compatible_surface = surface
    2 | 4 | 8
    static-typify request_adapter_callback wgpu.AdapterId voidstar
    &adapter

let device =
    wgpu.adapter_request_device adapter
        reftoptr
            local wgpu.DeviceDescriptor
                extensions =
                    typeinit
                        anisotropic_filtering = false
                limits =
                    typeinit
                        max_bind_groups = 1
        null

let vertex_shader =
    wgpu.device_create_shader_module device
        reftoptr
            local wgpu.ShaderModuleDescriptor
                code =
                    wgpu.U32Array
                        bitcast (vert as rawstring) (pointer u32)
                        (countof vert) // 4

let fragment_shader =
    wgpu.device_create_shader_module device
        reftoptr
            local wgpu.ShaderModuleDescriptor
                code =
                    wgpu.U32Array
                        bitcast (frag as rawstring) (pointer u32)
                        (countof frag) // 4

local bind_group_layout =
    wgpu.device_create_bind_group_layout device
        reftoptr
            local wgpu.BindGroupLayoutDescriptor
                label = "bind group layout"
                entries = null
                entries_length = 0

let bind_group =
    wgpu.device_create_bind_group device
        reftoptr
            local wgpu.BindGroupDescriptor
                label = "bind group"
                layout = bind_group_layout
                entries = null
                entries_length = 0

let pipeline_layout =
    wgpu.device_create_pipeline_layout device
        reftoptr
            local wgpu.PipelineLayoutDescriptor
                bind_group_layouts = &bind_group_layout
                bind_group_layouts_length = 1

let render_pipeline =
    wgpu.device_create_render_pipeline device
        reftoptr
            local wgpu.RenderPipelineDescriptor
                layout = pipeline_layout
                vertex_stage =
                    wgpu.ProgrammableStageDescriptor
                        module = vertex_shader
                        entry_point = "main"
                fragment_stage =
                    reftoptr
                        local wgpu.ProgrammableStageDescriptor
                            module = fragment_shader
                            entry_point = "main"
                rasterization_state =
                    reftoptr
                        local wgpu.RasterizationStateDescriptor
                            front_face = wgpu.FrontFace.Ccw
                            cull_mode = wgpu.CullMode.None
                            depth_bias = 0
                            depth_bias_slope_scale = 0.0
                            depth_bias_clamp = 0.0
                primitive_topology = wgpu.PrimitiveTopology.TriangleList
                color_states =
                    reftoptr
                        local wgpu.ColorStateDescriptor
                            format = wgpu.TextureFormat.Bgra8Unorm
                            alpha_blend =
                                wgpu.BlendDescriptor
                                    src_factor = wgpu.BlendFactor.One
                                    dst_factor = wgpu.BlendFactor.Zero
                                    operation = wgpu.BlendOperation.Add
                            color_blend =
                                wgpu.BlendDescriptor
                                    src_factor = wgpu.BlendFactor.One
                                    dst_factor = wgpu.BlendFactor.Zero
                                    operation = wgpu.BlendOperation.Add
                            write_mask = wgpu.ColorWrite.ALL
                color_states_length = 1
                depth_stencil_state = null
                vertex_state =
                    wgpu.VertexStateDescriptor
                        index_format = wgpu.IndexFormat.Uint16
                        vertex_buffers = null
                        vertex_buffers_length = 0
                sample_count = 1

local prev_width = 0
local prev_height = 0
glfwGetWindowSize window &prev_width &prev_height

local swap_chain =
    wgpu.device_create_swap_chain device surface
        reftoptr
            local wgpu.SwapChainDescriptor
                usage = wgpu.TextureUsage.OUTPUT_ATTACHMENT
                format = wgpu.TextureFormat.Bgra8Unorm
                width =
                    prev_width as u32
                height =
                    prev_height as u32
                present_mode = wgpu.PresentMode_Fifo

let queue =
    wgpu.device_get_default_queue device

while (not (glfwWindowShouldClose window))
    local width = 0
    local height = 0
    glfwGetWindowSize window &width &height
    if (width != prev_width or height != prev_height)
        prev_width = width
        prev_height = height

        swap_chain =
            wgpu.device_create_swap_chain device surface
                reftoptr
                    local wgpu.SwapChainDescriptor
                        usage = wgpu.TextureUsage.OUTPUT_ATTACHMENT
                        format = wgpu.TextureFormat.Bgra8Unorm
                        width = (width as u32)
                        height = (height as u32)
                        present_mode = wgpu.PresentMode_Fifo

    let next_texture =
        wgpu.swap_chain_get_next_texture swap_chain

    let cmd_encoder =
        wgpu.device_create_command_encoder device
            reftoptr
                local wgpu.CommandEncoderDescriptor
                    label = "command encoder"

    local color_attachment : wgpu.RenderPassColorAttachmentDescriptor
        attachment = next_texture.view_id
        load_op = wgpu.LoadOp.Clear
        store_op = wgpu.StoreOp.Store
        clear_color =
            wgpu.Color.GREEN

    local desc : wgpu.RenderPassDescriptor
        color_attachments =
            reftoptr color_attachment
        color_attachments_length = 1
        depth_stencil_attachment = null

    let rpass = (wgpu.command_encoder_begin_render_pass cmd_encoder &desc)

    wgpu.render_pass_set_pipeline rpass render_pipeline
    wgpu.render_pass_set_bind_group rpass 0 bind_group null 0
    wgpu.render_pass_draw rpass 3 1 0 0
    wgpu.render_pass_end_pass rpass
    local cmd_buf =
        wgpu.command_encoder_finish cmd_encoder null
    wgpu.queue_submit queue &cmd_buf 1

    wgpu.swap_chain_present swap_chain

    glfwPollEvents;

glfwDestroyWindow window
glfwTerminate;

