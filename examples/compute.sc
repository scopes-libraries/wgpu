import ..init

let wgpu = init

using import utils.utils

load-library "/usr/local/lib/libwgpu_native.so"
run-stage;

include "stdlib.h"
include "stdio.h"

fn read-buffer-map (status data userdata)
    if (status == wgpu.BufferMapAsyncStatus.Success)
        let times =
            bitcast data (pointer u32)
        print
            ..
                "Times: ["
                tostring
                    times @ 0
                ", "
                tostring
                    times @ 1
                ", "
                tostring
                    times @ 2
                ", "
                tostring
                    times @ 3
                "]"

let read-buffer-map =
    static-typify read-buffer-map wgpu.BufferMapAsyncStatus (pointer u8) (mutable pointer u8)

fn read_file (name)
    let file =
        fopen name "rb"
    fseek file 0 SEEK_END
    let length =
        (ftell file) as u64
    let bytes =
        malloc-array u8 length
    fseek file 0 SEEK_SET
    fread bytes 1 length file
    fclose file
    wgpu.ByteArray
        keyed bytes length

let argc argv =
    launch-args;

if (argc != 6)
    error "You must pass 4 positive integers!"

let numbers =
    arrayof i32
        strtoul (argv @ 2) null 0
        strtoul (argv @ 3) null 0
        strtoul (argv @ 4) null 0
        strtoul (argv @ 5) null 0

let size =
    sizeof (typeof numbers)

let instance =
    wgpu.Instance;

let adapter =
    wgpu.Adapter instance
        local wgpu.AdapterDescriptor
            power_preference = wgpu.PowerPreference.LowPower

let device =
    wgpu.Device adapter
        local wgpu.DeviceDescriptor

let staging-buffer staging-memory =
    wgpu.Buffer device
        local wgpu.BufferDescriptor
            size = size
            usage = wgpu.BufferUsage.MAP_READ
        i32

staging-memory @ 0 = numbers @ 0
staging-memory @ 1 = numbers @ 1
staging-memory @ 2 = numbers @ 2
staging-memory @ 3 = numbers @ 3

'unmap staging-buffer

let storage-buffer =
    wgpu.Buffer device
        local wgpu.BufferDescriptor
            size = size
            usage = wgpu.BufferUsage.STORAGE

local bind-group-layout =
    wgpu.BindGroupLayout device
        local wgpu.BindGroupLayoutDescriptor
            bindings = 
                local wgpu.BindGroupLayoutBinding
                    binding = 0
                    visibility = wgpu.ShaderStage.COMPUTE
                    ty = wgpu.BindingType.StorageBuffer

let resource =
    wgpu.BindingResource.Buffer
        local wgpu.BufferBinding
            buffer = storage-buffer
            size = size
            offset = 0

let bind-group =
    wgpu.BindGroup device
        local wgpu.BindGroupDescriptor
            layout = bind-group-layout
            bindings = 
                local wgpu.BindGroupBinding
                    binding = 0
                    resource = resource

local pipeline-layout =
    wgpu.PipelineLayout device
        local wgpu.PipelineLayoutDescriptor
            bind_group_layouts = bind-group-layout

let shader-module =
    wgpu.ShaderModule device
        local wgpu.ShaderModuleDescriptor
            code =
                read_file "collatz.comp.spv"

let compute-pipeline =
    wgpu.ComputePipeline device
        local wgpu.ComputePipelineDescriptor
            layout = pipeline-layout
            compute_stage =
                local wgpu.PipelineStageDescriptor
                    module = shader-module
                    entry_point = "main"

let encoder =
    wgpu.CommandEncoder device
        local wgpu.CommandEncoderDescriptor
            todo = 0

'copy-buffer-to-buffer encoder staging-buffer 0 storage-buffer 0 size

let command-pass =
    wgpu.ComputePass encoder
'set-pipeline command-pass compute-pipeline

'set-bind-group command-pass 0 bind-group
'dispatch command-pass 4 1 1
let command-buffer =
    wgpu.CommandBuffer command-pass

'copy-buffer-to-buffer encoder storage-buffer 0 staging-buffer 0 size

let queue =
    wgpu.Queue device

local command-buffer : wgpu.CommandBuffer encoder

'submit queue command-buffer

'map-read-async staging-buffer 0 size read-buffer-map null

'poll device true

