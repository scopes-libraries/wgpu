using import .helpers
using import .native

inline single (create-function super)
    create-function
        'native super

do
    let
        BindGroupLayoutEntry
        BufferAddress
        BufferCopyView
        Extent3d
        Limits
        Origin3d
        ShaderLocation
        TextureCopyView

    let ShaderStage BufferUsage TextureUsage ColorWrite TextureAspectFlags

    let Color

    let U32Array
    typedef+ U32Array
        # this is allowed, since this method didn't exist before
        inline __rimply (vT T)
            static-if (vT == string)
                inline "U32Array-rimply" (self)
                    U32Array
                        bitcast (self as rawstring) (pointer u32)
                        (countof self) // 4

    # device descriptor
    let
        Extensions
        Limits

    locals;

