using import .native

do
    let AddressMode
        BindingType
        BlendFactor
        BlendOperation
        BufferMapAsyncStatus
        CompareFunction
        CullMode
        FilterMode
        FrontFace
        IndexFormat
        InputStepMode
        LoadOp
        LogLevel
        PowerPreference
        PresentMode
        PrimitiveTopology
        StencilOperation
        StoreOp
        SwapChainStatus
        TextureAspect
        TextureComponentType
        TextureDimension
        TextureFormat
        TextureViewDimension
        VertexFormat

    locals;
